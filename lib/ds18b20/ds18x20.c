#include <msp430.h>
#include "ds18x20.h"


// convert temperature to 3 BYTE hex character array

int32_t GetTempData(void){
    unsigned int temp;
  	ResetDS1820();
    WriteByte(DS1820_SKIP_ROM);
	WriteByte(DS1820_CONVERT_T);
	DS1820_HI();
    delay_ms(750);
    ResetDS1820();
    WriteByte(DS1820_SKIP_ROM);
    WriteByte(DS1820_READ_SCRATCHPAD);
    temp = ReadDS1820();

    int16_t stemp = (int16_t)temp;
    return((int32_t)stemp*625/10000);
}

uint16_t GetTempRaw(void){
    unsigned int temp;
  	ResetDS1820();
    WriteByte(DS1820_SKIP_ROM);
	WriteByte(DS1820_CONVERT_T);
	DS1820_HI();
    delay_ms(750);
    ResetDS1820();
    WriteByte(DS1820_SKIP_ROM);
    WriteByte(DS1820_READ_SCRATCHPAD);
    temp = ReadDS1820();

    return temp;
}

char TO_HEX(uint16_t i){
	if (i <=9){
		return '0'+i;
	} else {
		return 'A' - 10 + i;
	}
}

char *GetTempHex(void){
	uint16_t temp = GetTempRaw();
	static char val[5];
	if(temp <= 0xFFFF){
		val[0] = TO_HEX( ( (temp & 0xF000) >> 12) );
		val[1] = TO_HEX( ( (temp & 0x0F00) >> 8));
		val[2] = TO_HEX( ( (temp & 0x00F0) >> 4));
		val[3] = TO_HEX( ( (temp & 0x000F) ));
		val[4] = '\0';
	} else {
		val[0] = '0';
		val[1] = '0';
		val[2] = '0';
		val[3] = '0';
		val[4] = '\0';
	}
	return val;
}

int32_t GetTempData10(void){
    unsigned int temp;
  	ResetDS1820();
    WriteByte(DS1820_SKIP_ROM);
	WriteByte(DS1820_CONVERT_T);
	DS1820_HI();
    delay_ms(750);
    ResetDS1820();
    WriteByte(DS1820_SKIP_ROM);
    WriteByte(DS1820_READ_SCRATCHPAD);
    temp = ReadDS1820();

    int16_t stemp = (int16_t)temp;
    return((int32_t)stemp*6250/10000);
}

//#####################################################################

uint16_t ReadDS1820 ( void ){
	unsigned int i;
	uint16_t byte = 0;
	for(i = 16; i > 0; i--){
		byte >>= 1;
		if (ReadBit()) {
			byte |= 0x8000;
		}
	}
	return byte;
}

//#####################################################################


void WriteByte (uint8_t byte){ // writes a byte
	int i;
	for(i = 0; i < 8; i++){
		WriteBit(byte & 1);
		byte >>= 1;
	}
}

//#####################################################################


uint8_t ReadByte(){
	unsigned int i;
	  uint8_t byte = 0;
	  for(i = 0; i < 8; i++){
	    byte >>= 1;
	    if (ReadBit()) byte |= 0x80;
	  }
	  return byte;
}



//#####################################################################


/* ReadBit
 * Steps for master to issue a read request to slave device on bus aka milk slave device
	* pull bus low
	* hold for 5us
	* release bus
	* wait for 45us for recovery (46us+5us=61us)
*/
unsigned int ReadBit (void){
	int bit=0;
	delay_us (1);                      // Recovery
	DS1820_LO();                       // Drive bus low
	delay_us (5);                      // hold min 1us
	DS1820_RELEASE_BUS();              // release bus. set port in input mode
	delay_us (10);                     // wait for slave to drive port either high or low 15us window
	if(DS1820_INPUT()){// read bus
		bit=1; 						   // if read high set bit high
	}
	delay_us (46); 					   // recovery time slot
	return bit;
}

//#####################################################################


void WriteBit(int bit)
{
	delay_us(1);                // recovery, min 1us
	DS1820_HI();
	if (bit) {
		DS1820_LO();
		delay_us(5);            // max 15us
		DS1820_RELEASE_BUS();	// input
		delay_us(56);
	} else {
		DS1820_LO();
		delay_us(60);           // min 60us
		DS1820_RELEASE_BUS();	// input
		delay_us(1);
	}
 }



//#####################################################################


/* ResetDS1820
 * Steps to reset one wire bus
	* Pull bus low
	* hold condition for 480us
	* release bus
	* wait for 60us
	* read bus
	* if bus low then device present set / return var accordingly
	* wait for balance period (480-60)
*/
unsigned int ResetDS1820 ( void ){
	DS1820_LO();                       // Drive bus low
	delay_us (500);                    // hold for 480us //try 500
	DS1820_RELEASE_BUS();              // release bus. set port in input mode
	delay_us(80);                      // wait for slave to pull bus low //try 80 or 40
	if(DS1820_INPUT()) return 1;
	delay_us (300);                    // slave TX presence pulse 60-240us
	if(!DS1820_INPUT()) return 2;
	return 0;
}

//#####################################################################


void DS1820_HI(){
	DS1820_DIR |= DS1820_DATA_IN_PIN;
	//DS1820_REN &= ~DS1820_DATA_IN_PIN;
	DS1820_OUT |= DS1820_DATA_IN_PIN;
}

//#####################################################################


void DS1820_LO(){
	DS1820_DIR |= DS1820_DATA_IN_PIN;
	//DS1820_REN &= ~DS1820_DATA_IN_PIN;
	DS1820_OUT &= ~DS1820_DATA_IN_PIN;
}

//#####################################################################


void DS1820_RELEASE_BUS(){
	DS1820_DIR &= ~DS1820_DATA_IN_PIN;
	//DS1820_REN |= DS1820_DATA_IN_PIN;
	DS1820_OUT |= DS1820_DATA_IN_PIN;
}

//#####################################################################


unsigned int DS1820_INPUT(){
	return (DS1820_IN & DS1820_DATA_IN_PIN);
}

//#####################################################################


void InitDS18B20(void){                 //General GPIO Defines
	DS1820_DIR |= (DS1820_VCC + DS1820_GND);
	DS1820_OUT |= DS1820_VCC;
	DS1820_OUT &= ~DS1820_GND;
}

//#####################################################################


void delay_ms(int ms){
	while (ms--){
		__delay_cycles(16000); // set for 16Mhz; change it to 1000 for 1 Mhz
	}
}

//#####################################################################


void delay_us(int us){
	while (us--){
		//__delay_cycles(16);     // set for 16Mhz; change it to 1000 for 1 Mhz
		__delay_cycles(8);     // set for 16Mhz; change it to 1000 for 1 Mhz
	}
}

