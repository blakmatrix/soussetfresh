// ds18x20.h - DS18B20 functions
#ifndef DS18X20_H_
#define DS18X20_H_
#include <inttypes.h>

#define DS1820_OUT 					P2OUT
#define DS1820_DIR 					P2DIR
#define DS1820_SEL					P2SEL
#define DS1820_REN					P2REN
#define DS1820_IN					P2IN
#define DS1820_DATA_IN_PIN          BIT4 // 2.4
#define DS1820_VCC			        BIT3 // not used
#define DS1820_GND          		BIT1 // not used

#define DS1820_SKIP_ROM             0xCC
#define DS1820_READ_SCRATCHPAD      0xBE
#define DS1820_CONVERT_T            0x44


int32_t GetTempData(void);
uint16_t GetTempRaw(void);
int32_t GetTempData10(void);
char TO_HEX(uint16_t i);
char *GetTempHex(void);
uint16_t ReadDS1820 ( void );
void WriteByte (uint8_t);
uint8_t ReadByte(void);
unsigned int ReadBit (void);
void WriteBit(int);
unsigned int ResetDS1820 ( void );
void DS1820_HI();
void DS1820_LO();
void DS1820_RELEASE_BUS();
unsigned int DS1820_INPUT();
void InitDS18B20(void);
void delay_ms(int ms);
void delay_us(int us);

#endif /*DS18X20_H_*/
