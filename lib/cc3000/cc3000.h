/**************************************************************************/
/*! 
  @file     cc3000.h
  @author   blakmatrix (Farrin Reid)
*/
/**************************************************************************/

#ifndef CC3000_H
#define CC3000_H

#include <cc3000_common.h>
#include <wlan.h>
#include <netapp.h>
#include <spi.h>

#define WLAN_CONNECT_TIMEOUT 10000  // how long to wait, in milliseconds
#define RXBUFFERSIZE  64 // how much to buffer on the incoming side
#define TXBUFFERSIZE  32 // how much to buffer on the outgoing side

#define WIFI_ENABLE 1
#define WIFI_DISABLE 0
#define WIFI_STATUS_CONNECTED 1

typedef struct Result_Struct
{
	uint32_t	num_networks;
	uint32_t 	scan_status;
	uint8_t 	rssiByte;
	uint8_t 	Sec_ssidLen;
	uint16_t 	time;
	uint8_t 	ssid_name[32];
	uint8_t 	bssid[6];
} ResultStruct_t;  	/**!ResultStruct_t : data struct to store SSID scan results */

/* Enum for wlan_ioctl_statusget results */
typedef enum 
{
	STATUS_DISCONNECTED = 0,
	STATUS_SCANNING     = 1,
	STATUS_CONNECTING   = 2,
	STATUS_CONNECTED    = 3
} status_t;

enum cc3000StateEnum
{
    CC3000_UNINIT           = 0x01, // CC3000 Driver Uninitialized
    CC3000_INIT             = 0x02, // CC3000 Driver Initialized
    CC3000_ASSOC            = 0x04, // CC3000 Associated to AP
    CC3000_IP_ALLOC         = 0x08, // CC3000 has IP Address
    CC3000_SERVER_INIT      = 0x10, // CC3000 Server Initialized
    CC3000_CLIENT_CONNECTED = 0x20  // CC3000 Client Connected to Server
};







char InitCC3000(uint8_t patchReq);
void reboot(uint8_t patch);
void stop(void);
char disconnect(void);
char deleteProfiles(void);
char getMacAddress(uint8_t address[6]);
char setMacAddress(uint8_t address[6]);

unsigned char *returnIPAddress();
char connectOpen(const char *ssid);
void connectToAP(const char *ssid, const char *key, uint8_t secmode);
char checkConnected(void);
char checkDHCP(void);
char checkSmartConfigFinished(void);

uint32_t connectTCP(uint32_t destIP, uint16_t destPort);
uint32_t connectUDP(uint32_t destIP, uint16_t destPort);

void CC3000_Client(void);
void CC3000_Client_sock(uint16_t s);
char CC3000_Client_connected(void);
char startSmartConfig(char enableAES);

int16_t CC3000_Client_write_int16(const void *buf, uint16_t len, uint32_t flags);
size_t  CC3000_Client_write(uint8_t c);
int16_t CC3000_Client_read_int16(void *buf, uint16_t len, uint32_t flags);
uint8_t CC3000_Client_read_uint8(void);
int32_t CC3000_Client_close(void);
uint8_t CC3000_Client_available(void);

void CC3000_UsynchCallback(long lEventType, char * data, unsigned char length);

char *sendDriverPatch(unsigned long *Length);
char *sendBootLoaderPatch(unsigned long *Length);
char *sendWLFWPatch(unsigned long *Length);

//#####################################################################


void CC3000_delay_ms(int ms);
void CC3000_delay_us(int us);

/* Functions that aren't available with the tiny driver */
#ifndef CC3000_TINY_DRIVER
char getIPAddress(uint32_t *retip, uint32_t *netmask, uint32_t *gateway, uint32_t *dhcpserv, uint32_t *dnsserv);
char scanSSIDs(uint32_t time);
char getFirmwareVersion(uint8_t *major, uint8_t *minor);

char connectSecure(const char *ssid, const char *key, int32_t secMode);
char getIPConfig(tNetappIpconfigRetArgs *ipConfig);

status_t getStatus();
uint16_t startSSIDscan();
uint16_t ping(uint32_t ip, uint8_t attempts, uint16_t timeout, uint8_t size);
uint16_t getHostByName(char *hostname, uint32_t *ip);

#endif


#endif //CC3000_H
