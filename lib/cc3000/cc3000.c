/**************************************************************************/
/*!
  @file     cc3000.c
  @author   blakmatrix (Farrin Reid)
*/
/**************************************************************************/
#include "cc3000.h"
#include "board.h"
#include "LED.h"
#include <spi.h>
#include <msp430g2553.h>
#include <cc3000_common.h>
#include <evnt_handler.h>
#include <hci.h>
#include <netapp.h>
#include <nvmem.h>
#include <security.h>
#include <socket.h>
#include <wlan.h>

/***********************/

uint8_t pingReportnum;
netapp_pingreport_args_t pingReport;

#define CC3000_SUCCESS                  (0)
#define NETAPP_IPCONFIG_MAC_OFFSET		(20)
#define CHECK_SUCCESS(func,Notify,errorCode)  {if ((func) != CC3000_SUCCESS) { if (CC3KPrinter != 0) CC3KPrinter->println(F(Notify)); return errorCode;}}

#define MAXSSID							(32)
#define MAXLENGTHKEY 					(32)  /* Cleared for 32 bytes by TI engineering 29/08/13 */

#define MAX_SOCKETS 					(4)  // can change this
#define LED_RED 						BIT0
#define LED_GREEN 						BIT1
#define LED_BLUE 						BIT2
char closed_sockets[MAX_SOCKETS] = {0, 0, 0, 0};

/* *********************************************************************** */
/*                                                                         */
/* PRIVATE FIELDS (SmartConfig)                                            */
/*                                                                         */
/* *********************************************************************** */
volatile unsigned long ulSmartConfigFinished, 
                       ulCC3000Connected,
                       ulCC3000DHCP,
                       OkToDoShutDown, 
                       ulCC3000DHCP_configured;
volatile unsigned char ucStopSmartConfig, _initialised;
volatile long 		   ulSocket;

char _deviceName[] = "SousSet device 1.0";
char _cc3000_prefix[] = { 'T', 'T', 'T' };
const unsigned char _smartConfigKey[] = { 0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37,
                                          0x38, 0x39, 0x30, 0x31, 0x32, 0x33, 0x34, 0x35 };
                                          // AES key for smart config = "0123456789012345"

unsigned char pucCC3000_Rx_Buffer[4];

uint8_t _rx_buf[RXBUFFERSIZE], _rx_buf_idx;
char    _tx_buf[TXBUFFERSIZE];
int16_t bufsiz;
int16_t _socket;


/**************************************************************************/
/*!
    @brief    Scans for SSID/APs in the CC3000's range

    @note     This command isn't available when the CC3000 is configured
              in 'CC3000_TINY_DRIVER' mode

    @returns  False if an error occured!
*/
/**************************************************************************/
#ifndef CC3000_TINY_DRIVER
char scanSSIDs(uint32_t time)
{
  const unsigned long intervalTime[16] = { 2000, 2000, 2000, 2000,  2000,
    2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000, 2000 };

  if (!_initialised)
  {
    return 0;
  }

  // We can abort a scan with a time of 0
  if (time)
  {
    if (CC3KPrinter != 0) {
      CC3KPrinter->println(F("Started AP/SSID scan\n\r"));
    }
  }


  return wlan_ioctl_set_scan_params(time, 20, 100, 5, 0x7FF, -120, 0, 300,(unsigned long * ) &intervalTime);

}
#endif



/**************************************************************************/
/*!
    @brief  Setups the HW
*/
/**************************************************************************/
char InitCC3000(uint8_t patchReq)
{
	LED_ON(LED_BLUE);
	  _initialised = 0;
	  ulCC3000DHCP          = 0;
	  ulCC3000Connected     = 0;
	  ulSocket              = 0;
	  ulSmartConfigFinished = 0;

	  // Init GPIO's
	  pio_init();


	  init_spi();


	  wlan_init(CC3000_UsynchCallback,
				sendWLFWPatch, sendDriverPatch, sendBootLoaderPatch,
				ReadWlanInterruptPin,
				WlanInterruptEnable,
				WlanInterruptDisable,
				WriteWlanPin);

	  wlan_start(patchReq);

	  // Uncomment below if you want device to reset on InitCC3000
	  //wlan_ioctl_set_connection_policy(0, 0, 0);
	  //wlan_ioctl_del_profile(255);

	  wlan_set_event_mask(	HCI_EVNT_WLAN_UNSOL_INIT 			|
							//HCI_EVNT_WLAN_ASYNC_PING_REPORT	|// we want ping reports
							//HCI_EVNT_BSD_TCP_CLOSE_WAIT 		|
							//HCI_EVNT_WLAN_TX_COMPLETE 		|
							HCI_EVNT_WLAN_KEEPALIVE);

  // Why does calling SSID scan first improve reliability?
  //scanSSIDs(0);
  
  _initialised = 1;
  LED_OFF(LED_BLUE);
  
  return 1;
}



/**************************************************************************/
/*!
    @brief   Reboot CC3000 (stop then start)
*/
/**************************************************************************/
void reboot(uint8_t patch)
{
  if (!1)//_initialised
  {
    return;
  }

  wlan_stop();
  CC3000_delay_ms(5000);
  wlan_start(patch);
}

/**************************************************************************/
/*!
    @brief   Stop CC3000
*/
/**************************************************************************/
void stop(void)
{
  if (!_initialised)
  {
    return;
  }

  wlan_stop();
}

/**************************************************************************/
/*!
    @brief  Disconnects from the network

    @returns  False if an error occured!
*/
/**************************************************************************/
char disconnect(void)
{
  if (!_initialised)
  {
    return 0;
  }

  long retVal = wlan_disconnect();

  return retVal != 0 ? 0 : 1;
}

/**************************************************************************/
/*!
    @brief   Deletes all profiles stored in the CC3000

    @returns  False if an error occured!
*/
/**************************************************************************/
char deleteProfiles(void)
{
  if (!_initialised)
  {
    return 0;
  }

  wlan_ioctl_set_connection_policy(0, 0, 0);
  wlan_ioctl_del_profile(255);


  return 1;
}

/**************************************************************************/
/*!
    @brief    Reads the MAC address

    @param    address  Buffer to hold the 6 byte Mac Address

    @returns  False if an error occured!
*/
/**************************************************************************/
char getMacAddress(uint8_t address[6])
{
  if (!_initialised)
  {
    return 0;
  }

  nvmem_read(NVMEM_MAC_FILEID, 6, 0, address);

  return 1;
}

/**************************************************************************/
/*!
    @brief    Sets a new MAC address

    @param    address   Buffer pointing to the 6 byte Mac Address

    @returns  False if an error occured!
*/
/**************************************************************************/
char setMacAddress(uint8_t address[6])
{
  if (!_initialised)
  {
    return 0;
  }

  if (address[0] == 0)
  {
    return 0;
  }

  netapp_config_mac_adrress(address);

  wlan_stop();
  CC3000_delay_ms(200);
  wlan_start(0);

  return 1;
}

/**************************************************************************/
/*!
    @brief   Reads the current IP address

    @returns  False if an error occured!
*/
/**************************************************************************/
#ifndef CC3000_TINY_DRIVER
char getIPAddress(uint32_t *retip, uint32_t *netmask, uint32_t *gateway, uint32_t *dhcpserv, uint32_t *dnsserv)
{
  if (!_initialised) return 0;

  tNetappIpconfigRetArgs ipconfig;
  netapp_ipconfig(&ipconfig);

  /* If byte 1 is 0 we don't have a valid address */
  if (ipconfig.aucIP[3] == 0) return 0;

  memcpy(retip,    ipconfig.aucIP   ,  4);
  memcpy(netmask,  ipconfig.aucIP +4,  4);
  memcpy(gateway,  ipconfig.aucIP +8,  4);
  memcpy(dhcpserv, ipconfig.aucIP +12, 4);
  memcpy(dnsserv,  ipconfig.aucIP +16, 4);

  return 1;
}
#endif

unsigned char *returnIPAddress()
{
	return ( unsigned char *)pucCC3000_Rx_Buffer;
}

/**************************************************************************/
/*!
    @brief    Gets the two byte ID for the firmware patch version

    @note     This command isn't available when the CC3000 is configured
              in 'CC3000_TINY_DRIVER' mode

    @returns  False if an error occured!
*/
/**************************************************************************/
#ifndef CC3000_TINY_DRIVER
char getFirmwareVersion(uint8_t *major, uint8_t *minor)
{
  uint8_t fwpReturn[2];

  if (!_initialised)
  {
    return 0;
  }

  nvmem_read_sp_version(fwpReturn);

  *major = fwpReturn[0];
  *minor = fwpReturn[1];

  return 1;
}
#endif

/**************************************************************************/
/*!
    @Brief   Prints out the current status flag of the CC3000

    @note     This command isn't available when the CC3000 is configured
              in 'CC3000_TINY_DRIVER' mode
*/
/**************************************************************************/
#ifndef CC3000_TINY_DRIVER
status_t getStatus()
{
  if (!_initialised)
  {
    return STATUS_DISCONNECTED;
  }

  long results = wlan_ioctl_statusget();

  switch(results)
  {
    case 1:
      return STATUS_SCANNING;
      break;
    case 2:
      return STATUS_CONNECTING;
      break;
    case 3:
      return STATUS_CONNECTED;
      break;
    case 0:
    default:
      return STATUS_DISCONNECTED;
      break;
  }
}
#endif

/**************************************************************************/
/*!
    @brief    Calls listSSIDs and then displays the results of the SSID scan

              For the moment we only list these via CC3KPrinter->print since
              this can consume a lot of memory passing all the data
              back with a buffer

    @note     This command isn't available when the CC3000 is configured
              in 'CC3000_TINY_DRIVER' mode

    @returns  False if an error occured!
*/
/**************************************************************************/
#ifndef CC3000_TINY_DRIVER

ResultStruct_t SSIDScanResultBuff;


uint16_t startSSIDscan() {
  uint16_t   index = 0;

  if (!_initialised)
  {
    return 0;
  }

  // Setup a 4 second SSID scan
  if (!scanSSIDs(4000))
  {
    // SSID scan failed
    return 0;
  }

  // Wait for results
  delay(4500);

  wlan_ioctl_get_scan_results(0, (uint8_t* ) &SSIDScanResultBuff);

  index = SSIDScanResultBuff.num_networks;
  return index;
}

void stopSSIDscan(void) {

  // Stop scanning
  scanSSIDs(0);
}

uint8_t getNextSSID(uint8_t *rssi, uint8_t *secMode, char *ssidname) {
    uint8_t valid = (SSIDScanResultBuff.rssiByte & (~0xFE));
    *rssi = (SSIDScanResultBuff.rssiByte >> 1);
    *secMode = (SSIDScanResultBuff.Sec_ssidLen & (~0xFC));
    uint8_t ssidLen = (SSIDScanResultBuff.Sec_ssidLen >> 2);
    strncpy(ssidname, (char *)SSIDScanResultBuff.ssid_name, ssidLen);
    ssidname[ssidLen] = 0;

    CHECK_SUCCESS(wlan_ioctl_get_scan_results(0, (uint8_t* ) &SSIDScanResultBuff),
                  "Problem with the SSID scan results", false);
    return valid;
}
#endif

/**************************************************************************/
/*!
    @brief    Starts the smart config connection process

    @note     This command isn't available when the CC3000 is configured
              in 'CC3000_TINY_DRIVER' mode

    @returns  False if an error occured!
*/
/**************************************************************************/
//#ifndef CC3000_TINY_DRIVER
char startSmartConfig(char enableAES)
{
	//TODO: Setup WIFI Blink to indicate SMartconfig mode
  ulSmartConfigFinished = 0;
  ulCC3000Connected = 0;
  ulCC3000DHCP = 0;
  OkToDoShutDown=0;

  uint32_t   timeout = 0;

  if (!_initialised) {
    return 0;
  }

  // Reset all the previous configurations

  wlan_ioctl_set_connection_policy(WIFI_DISABLE, WIFI_DISABLE, WIFI_DISABLE);
  wlan_ioctl_del_profile(255);

  //Wait until CC3000 is disconnected
  while (ulCC3000Connected == WIFI_STATUS_CONNECTED) {
	  // ToDo: check for missed interruptions in case hardware missed falling edge
    wlan_disconnect();
    CC3000_delay_ms(100);
    hci_unsolicited_event_handler();
  }
  //LED_OFF(LED_BLUE+LED_GREEN);

  // Reset the CC3000
  wlan_stop();
  CC3000_delay_ms(1000);
  wlan_start(0);

#ifndef CC3000_UNENCRYPTED_SMART_CONFIG
  // create new entry for AES encryption key
  nvmem_create_entry(NVMEM_AES128_KEY_FILEID,16);
  
  // write AES key to NVMEM
  aes_write_key((unsigned char *)(&_smartConfigKey[0]));
#endif
  // Wait until CC3000 is disconnected
  wlan_smart_config_set_prefix((char *)&_cc3000_prefix);


  // Start the SmartConfig start process
  wlan_smart_config_start(0);
  // Wait for smart config process complete (event in CC3000_UsynchCallback)
  while (ulSmartConfigFinished == 0)
  {
	  // ToDo: check for missed interruptions in case hardware missed falling edge
    // waiting here for event SIMPLE_CONFIG_DONE
	LED_TOGGLE(LED_BLUE+LED_GREEN);//TOGGLE blue and green
    timeout+=100;
    if (timeout > 60000)   // ~60s
    {
      LED_ON(LED_BLUE+LED_RED);
      CC3000_delay_ms(1000);
      LED_OFF(LED_BLUE);
      return 0;
    }
    CC3000_delay_ms(100); // ms
  }
  LED_OFF(LED_BLUE+LED_GREEN);
// This will try and connect without key first, then with it?
#ifndef CC3000_UNENCRYPTED_SMART_CONFIG
  if (enableAES) {
    wlan_smart_config_process();
  }
#endif
  // Connect automatically to the AP specified in smart config settings
  wlan_ioctl_set_connection_policy(WIFI_DISABLE, WIFI_DISABLE, WIFI_ENABLE);
  
  // Reset the CC3000
  wlan_stop();
  CC3000_delay_ms(1000);
  wlan_start(0);
  
  // Mask out all non-required events
  wlan_set_event_mask(	HCI_EVNT_WLAN_KEEPALIVE 			|
                		HCI_EVNT_WLAN_UNSOL_INIT 			//|
                		//HCI_EVNT_WLAN_ASYNC_PING_REPORT	|
                		//HCI_EVNT_WLAN_TX_COMPLETE
                );
  

  // Wait for a connection
  timeout = 0;
  LED_ON(LED_BLUE); // blue when connecting
  while(!ulCC3000Connected)
  {

	  // ToDo: check for missed interruptions in case hardware missed falling edge
    if(timeout > WLAN_CONNECT_TIMEOUT) // ~20s
    {
      LED_ON(LED_BLUE+LED_RED);
      CC3000_delay_ms(1000);
      LED_OFF(LED_BLUE+LED_RED);
      return 0;
    }
    timeout += 100;
    CC3000_delay_ms(100);
  }
  //LED_OFF(LED_BLUE+LED_GREEN);
  
  // Wait for a DHCP
    while(!ulCC3000DHCP)
    {
      LED_TOGGLE(LED_BLUE);
      CC3000_delay_ms(500);
    }

  CC3000_delay_ms(1000);
  if (ulCC3000DHCP)
  {
    mdnsAdvertiser(1, (char *) _deviceName, strlen(_deviceName));
  }
  
  timeout = 0;
  LED_ON(LED_GREEN);
  while(timeout < 3000){ // Solid green with blinking blue pattern means we're connected YAY!
	  if(	timeout	==	200  ||
			timeout	== 	400  ||
			timeout	== 	600  ||
			timeout	==	1200 ||
			timeout	==	1400 ||
			timeout	==	1600 ||
			timeout	==	2200 ||
			timeout	==	2400 ||
			timeout	==	2600 //||
			//timeout	==	3200 ||
			//timeout	==	3400 ||
			){//timeout	==	3600){
		  LED_TOGGLE(LED_BLUE);
	  }
	  timeout+=100;
	  CC3000_delay_ms(100);
  }
  LED_OFF(LED_BLUE+LED_GREEN);

  return 1;
}

//#endif

/**************************************************************************/
/*!
    Connect to an unsecured SSID/AP(security)

    @param  ssid      The named of the AP to connect to (max 32 chars)
    @param  ssidLen   The size of the ssid name

    @returns  False if an error occured!
*/
/**************************************************************************/
char connectOpen(const char *ssid)
{
  if (!_initialised) {
    return 0;
  }

  #ifndef CC3000_TINY_DRIVER
    wlan_ioctl_set_connection_policy(0, 0, 0);
    delay(500);
    wlan_connect(WLAN_SEC_UNSEC,
					(const char*)ssid, strlen(ssid),
					0 ,NULL,0);
  #else
    wlan_connect((char*)ssid, strlen(ssid));
  #endif

  return 1;
}

//*****************************************************************************
//
//! CC3000_UsynchCallback
//!
//! @param  lEventType Event type
//! @param  data
//! @param  length
//!
//! @return none
//!
//! @brief  The function handles asynchronous events that come from CC3000
//!         device and operates a led for indicate
//
//*****************************************************************************
void CC3000_UsynchCallback(long lEventType, char * data, unsigned char length)
{
  if (lEventType == HCI_EVNT_WLAN_ASYNC_SIMPLE_CONFIG_DONE)
  {
    ulSmartConfigFinished = 1;
    ucStopSmartConfig     = 1;
  }

  if (lEventType == HCI_EVNT_WLAN_UNSOL_CONNECT)
  {
    ulCC3000Connected = 1;
  }

  if (lEventType == HCI_EVNT_WLAN_UNSOL_DISCONNECT)
  {
    ulCC3000Connected = 0;
    ulCC3000DHCP      = 0;
    ulCC3000DHCP_configured = 0;
  }
  
  if (lEventType == HCI_EVNT_WLAN_UNSOL_DHCP)
  {
    // Notes:
	// 1) IP config parameters are received swapped
	// 2) IP config parameters are valid only if status is OK, i.e. ulCC3000DHCP becomes 1

	// only if status is OK, the flag is set to 1 and the addresses are valid
	if ( *(data + NETAPP_IPCONFIG_MAC_OFFSET) == 0)
	{
		// Store IP for output purposes

		memcpy((char *)pucCC3000_Rx_Buffer, data, 4);

		ulCC3000DHCP = 1;
	}
	else
	{
		ulCC3000DHCP = 0;
	}
  }

  if (lEventType == HCI_EVENT_CC3000_CAN_SHUT_DOWN)
  {
    OkToDoShutDown = 1;
  }

  if (lEventType == HCI_EVNT_WLAN_ASYNC_PING_REPORT)
  {
    pingReportnum++;
    memcpy(&pingReport, data, length);
  }

  if (lEventType == HCI_EVNT_BSD_TCP_CLOSE_WAIT) {
    uint8_t socketnum;
    socketnum = data[0];
    if (socketnum < MAX_SOCKETS)
      closed_sockets[socketnum] = 1;
  }
}

/**************************************************************************/
/*!
    Connect to an SSID/AP(security)

    @note     This command isn't available when the CC3000 is configured
              in 'CC3000_TINY_DRIVER' mode

    @returns  False if an error occured!
*/
/**************************************************************************/
#ifndef CC3000_TINY_DRIVER
char connectSecure(const char *ssid, const char *key, int32_t secMode)
{
  int8_t  _key[MAXLENGTHKEY];
  uint8_t _ssid[MAXSSID];
  
  if (!_initialised) {
    return false;
  }
  
  if ( (secMode < 0) || (secMode > 3)) {
    return false;
  }

  if (strlen(ssid) > MAXSSID) {
    return false;
  }

  if (strlen(key) > MAXLENGTHKEY) {
    return false;
  }

  wlan_ioctl_set_connection_policy(0, 0, 0);
  delay(500);
  wlan_connect(	secMode, (char *)ssid, strlen(ssid),
				 NULL,
				 (unsigned char *)key, strlen(key));

  /* Wait for 'HCI_EVNT_WLAN_UNSOL_CONNECT' in CC3000_UsynchCallback */

  return true;
}
#endif

// Connect with timeout
void connectToAP(const char *ssid, const char *key, uint8_t secmode) {
  int16_t timer = WLAN_CONNECT_TIMEOUT;

  do {
    // ToDo: check for missed interruptions in case hardware missed falling edge
    // Setup a 4 second SSID scan
#ifndef CC3000_TINY_DRIVER
    scanSSIDs(4000);
    // Wait for results
    cc3000_delay_ms(4500);
    scanSSIDs(0);
#endif
    /* Attempt to connect to an access point */
    if ((secmode == 0) || (strlen(key) == 0)) {
      /* Connect to an unsecured network */
      if (! connectOpen(ssid)) {
        continue;
      }
    } else {
      /* NOTE: Secure connections are not available in 'Tiny' mode! */
#ifndef CC3000_TINY_DRIVER
      /* Connect to a secure network using WPA2, etc */
      if (! connectSecure(ssid, key, secmode)) {
        continue;
      }
#endif
    }

    /* Wait around a bit for the async connected signal to arrive or timeout */
    while ((timer > 0) && !checkConnected())
    {
    	// ToDo: check for missed interruptions in case hardware missed falling edge
    	CC3000_delay_ms(10);
    	timer -= 10;
    }
    if (timer <= 0) {
      //Timed out
    }
  } while (!checkConnected());
}


#ifndef CC3000_TINY_DRIVER
uint16_t ping(uint32_t ip, uint8_t attempts, uint16_t timeout, uint8_t size) {
  if (!_initialised)
  {
    return 0;
  }
  uint32_t revIP = (ip >> 24) | (ip >> 8) & 0xFF00 | (ip << 8) & 0xFF0000 | (ip << 24);

  pingReportnum = 0;
  pingReport.packets_received = 0;

  
  netapp_ping_send(&revIP, attempts, size, timeout);
  delay(timeout*attempts*2);

  if (pingReportnum) {
    return pingReport.packets_received;
  } else {
    return 0;
  }
}

#endif

#ifndef CC3000_TINY_DRIVER
uint16_t getHostByName(char *hostname, uint32_t *ip) {
  if (!_initialised) return 0;

  int16_t r = gethostbyname(hostname, strlen(hostname), ip);
  return r;
}
#endif

/**************************************************************************/
/*!
    Checks if the device is connected or not

    @returns  True if connected
*/
/**************************************************************************/
char checkConnected(void)
{
  return ulCC3000Connected ? 1 : 0;
}

/**************************************************************************/
/*!
    Checks if the DHCP process is complete or not

    @returns  True if DHCP process is complete (IP address assigned)
*/
/**************************************************************************/
char checkDHCP(void)
{
  return ulCC3000DHCP ? 1 : 0;
}

/**************************************************************************/
/*!
    Checks if the smart config process is finished or not

    @returns  True if smart config is finished
*/
/**************************************************************************/
char checkSmartConfigFinished(void)
{
  return ulSmartConfigFinished ? 1 : 0;
}


/**************************************************************************/
/*!
    Gets the IP config settings (if connected)

    @returns  True if smart config is finished
*/
/**************************************************************************/
#ifndef CC3000_TINY_DRIVER
char getIPConfig(tNetappIpconfigRetArgs *ipConfig)
{
  if (!_initialised)      return false;
  if (!ulCC3000Connected) return false;
  if (!ulCC3000DHCP)      return false;
  
  netapp_ipconfig(ipConfig);
  return 1;
}
#endif


/**************************************************************************/
/*!
    @brief  Quick socket test to pull contents from the web
*/
/**************************************************************************/
uint32_t connectTCP(uint32_t destIP, uint16_t destPort)
{
  sockaddr      socketAddress;
  uint32_t      tcp_socket;

  // Create the socket(s)
  tcp_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if ( tcp_socket == ((uint32_t)-1))
  {
    //"Failed to open socket"
    return 0;
  }
  //DONE

  // Try to open the socket
  memset(&socketAddress, 0x00, sizeof(socketAddress));
  socketAddress.sa_family = AF_INET;
  socketAddress.sa_data[0] = (destPort & 0xFF00) >> 8;  // Set the Port Number
  socketAddress.sa_data[1] = (destPort & 0x00FF);
  socketAddress.sa_data[2] = destIP >> 24;
  socketAddress.sa_data[3] = destIP >> 16;
  socketAddress.sa_data[4] = destIP >> 8;
  socketAddress.sa_data[5] = destIP;


  if (-1 == connect(tcp_socket, &socketAddress, sizeof(socketAddress)))
  {
    //"Connection error"
    closesocket(tcp_socket);
    return 0;
  }
  return tcp_socket;
}


uint32_t connectUDP(uint32_t destIP, uint16_t destPort)
{
  sockaddr      socketAddress;
  uint32_t      udp_socket;

  // Create the socket(s)
  // socket   = SOCK_STREAM, SOCK_DGRAM, or SOCK_RAW 
  // protocol = IPPROTO_TCP, IPPROTO_UDP or IPPROTO_RAW
  //"Creating socket... "
  udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (udp_socket == ((uint32_t)-1))
  {
    //"Failed to open socket")
    return 0;
  }

  // Try to open the socket
  memset(&socketAddress, 0x00, sizeof(socketAddress));
  socketAddress.sa_family = AF_INET;
  socketAddress.sa_data[0] = (destPort & 0xFF00) >> 8;  // Set the Port Number
  socketAddress.sa_data[1] = (destPort & 0x00FF);
  socketAddress.sa_data[2] = destIP >> 24;
  socketAddress.sa_data[3] = destIP >> 16;
  socketAddress.sa_data[4] = destIP >> 8;
  socketAddress.sa_data[5] = destIP;



  if (-1 == connect(udp_socket, &socketAddress, sizeof(socketAddress)))
  {
    //"Connection error")
    closesocket(udp_socket);
    return 0;
  }

  return udp_socket;
}


/**********************************************************************/
void CC3000_Client(void) {
  _socket = -1;
}

void CC3000_Client_sock(uint16_t s) {
  _socket = s; 
  bufsiz = 0;
  _rx_buf_idx = 0;
}

char CC3000_Client_connected(void) {
  if (_socket < 0) return 0;

  if (! CC3000_Client_available() && closed_sockets[_socket] == 1) {
    closesocket(_socket);
    closed_sockets[_socket] = 0;
    _socket = -1;
    return 0;
  }

  else return 1;
}

int16_t CC3000_Client_write_int16(const void *buf, uint16_t len, uint32_t flags)
{
  return send(_socket, buf, len, flags);
}


size_t CC3000_Client_write(uint8_t c)
{
  int32_t r;
  r = send(_socket, &c, 1, 0);
  if ( r < 0 ) return 0;
  return r;
}


int16_t CC3000_Client_read_int16(void *buf, uint16_t len, uint32_t flags)
{
  return recv(_socket, buf, len, flags);

}

int32_t CC3000_Client_close(void) {
  int32_t x = closesocket(_socket);
  _socket = -1;
  return x;
}

uint8_t CC3000_Client_read_uint8(void)
{
  while ((bufsiz <= 0) || (bufsiz == _rx_buf_idx)) {
	  // ToDo: check for missed interruptions in case hardware missed falling edge
    // buffer in some more data
    bufsiz = recv(_socket, _rx_buf, sizeof(_rx_buf), 0);
    if (bufsiz == -57) {
    	CC3000_Client_close();
      return 0;
    }
    _rx_buf_idx = 0;
  }
  uint8_t ret = _rx_buf[_rx_buf_idx];
  _rx_buf_idx++;
  return ret;
}

uint8_t CC3000_Client_available(void) {
  // not open!
  if (_socket < 0) return 0;

  if ((bufsiz > 0) // we have some data in the internal buffer
      && (_rx_buf_idx < bufsiz)) {  // we haven't already spit it all out
    return (bufsiz - _rx_buf_idx);
  }

  // do a select() call on this socket
  timeval timeout;
  fd_set fd_read;

  memset(&fd_read, 0, sizeof(fd_read));
  FD_SET(_socket, &fd_read);

  timeout.tv_sec = 0;
  timeout.tv_usec = 5000; // 5 millisec

  int16_t s = select(_socket+1, &fd_read, NULL, NULL, &timeout);
  if (s == 1) return 1;  // some data is available to read
  else return 0;  // no data is available
}

//*****************************************************************************
//
//! sendDriverPatch
//!
//! \param  pointer to the length
//!
//! \return none
//!
//! \brief  The function returns a pointer to the driver patch: since there is no patch yet -
//!				it returns 0
//
//*****************************************************************************
char *sendDriverPatch(unsigned long *Length)
{
    *Length = 0;
    return NULL;
}


//*****************************************************************************
//
//! sendBootLoaderPatch
//!
//! \param  pointer to the length
//!
//! \return none
//!
//! \brief  The function returns a pointer to the boot loader patch: since there is no patch yet -
//!				it returns 0
//
//*****************************************************************************
char *sendBootLoaderPatch(unsigned long *Length)
{
    *Length = 0;
    return NULL;
}

//*****************************************************************************
//
//! sendWLFWPatch
//!
//! \param  pointer to the length
//!
//! \return none
//!
//! \brief  The function returns a pointer to the FW patch: since there is no patch yet - it returns 0
//
//*****************************************************************************

char *sendWLFWPatch(unsigned long *Length)
{
    *Length = 0;
    return NULL;
}

//#####################################################################


void CC3000_delay_ms(int ms){
	while (ms--){
		__delay_cycles(16000); // set for 16Mhz; change it to 1000 for 1 Mhz
	}
}

//#####################################################################


void CC3000_delay_us(int us){
	while (us--){
		//__delay_cycles(16);     // set for 16Mhz; change it to 1000 for 1 Mhz
		__delay_cycles(8);     // set for 16Mhz; change it to 1000 for 1 Mhz
	}
}


