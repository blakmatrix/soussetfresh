// serial.h - Serial related functions

#ifndef SERIAL_H_
#define SERIAL_H_

#include <msp430.h>
#include <dispatcher.h>

void printLineTerminator(void);
void print( char * s);
int putchar(register int c);
void print_hexb(register unsigned char c);

void printLineTerminator(void){
	const unsigned char term[] = {'\f', '\r'};
	DispatcherUartSendPacket((unsigned char *)(term), sizeof(term));
}

/**
 * print() - like puts() but without a newline
 */
void print( char * s){
	printLineTerminator();
	do {
	DispatcherUartSendPacket((unsigned char *)s++, sizeof(char));
	} while (*s);
	printLineTerminator();


}



/**
 * putchar() - providing this function allows use of printf() on larger chips
 */
int putchar(register int c)
{
    //SoftSerial_xmit(c);
    return 0;
}

/**
 * print_hexb() - print uint8_t as hex
 */
void print_hexb(register unsigned char c)
{
    //static const unsigned char hextbl[]="0123456789ABCDEF";

    //SoftSerial_xmit(hextbl[c >> 4]);
    //SoftSerial_xmit(hextbl[c & 0x0F]);
}


#endif /*SERIAL_H_*/
