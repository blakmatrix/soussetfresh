/**************************************************************************/
/*!
  @file     cc3000.h
  @author   blakmatrix (Farrin Reid)
*/
/**************************************************************************/
// LED.c - LED related functions
//RED - 2.0
//GREEN - 2.1
//BLUE - 2.2
#include <msp430g2533.h>
#define LED_RED 	BIT0
#define LED_GREEN 	BIT1
#define LED_BLUE 	BIT2

void INIT_LEDS(){
	P2DIR |= (LED_GREEN+LED_RED+LED_BLUE ); // Set to output direction
	P2OUT &= ~(LED_GREEN+LED_RED+LED_BLUE); // Set the LEDs off
}

void LED_OFF(char leds){
	P2OUT &= ~leds;
}
void LED_TOGGLE(char leds){
	P2OUT ^= leds;
}
void LED_ON(char leds){
	LED_OFF(leds);
	LED_TOGGLE(leds);
}


