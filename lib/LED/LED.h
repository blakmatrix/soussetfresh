/**************************************************************************/
/*!
  @file     cc3000.h
  @author   blakmatrix (Farrin Reid)
*/
/**************************************************************************/
// LED.h - LED related functions
#ifndef LED_H_
#define LED_H_
#include <msp430g2533.h>
#include "LED.h"

void INIT_LEDS();
void LED_OFF(char leds);
void LED_ON(char leds);
void LED_TOGGLE(char leds);

#endif //LED_H_
