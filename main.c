#include <msp430g2533.h>
#include <dispatcher.h>
#include "ds18x20.h"
#include "serial.h"
#include "board.h"
#include "cc3000.h"
#include "LED.h"



#define LED_GREEN_B BIT0 // board
#define LED_RED_B	BIT6 // board
#define LED_RED 	BIT0
#define LED_GREEN 	BIT1
#define LED_BLUE 	BIT2

extern unsigned char pucCC3000_Rx_Buffer[4];

//extern unsigned char pucCC3000_Rx_Buffer[8 + 28];

/**
 * setup() - initialize timers and clocks
 */

void setup(){
	WDTCTL = WDTPW + WDTHOLD; // Stop watchdog timer
	P1DIR |= (LED_GREEN_B + LED_RED_B ); // Set P1.0 and P1.6 to output direction
	P1OUT &= ~(LED_GREEN_B + LED_RED_B); // Set the LEDs off

	// Initialize DS18B20
	InitDS18B20();

	// Initialize LEDs
	INIT_LEDS();
	LED_TOGGLE(LED_GREEN+LED_RED+LED_BLUE);
	__delay_cycles(3000000);
	LED_TOGGLE(LED_GREEN+LED_RED+LED_BLUE);

	// Initialize Hyper Terminal drivers
	DispatcherUARTConfigure();

	// Board + CC3000 Initialization start
	InitCC3000(0/* zero for no patch*/);
	print("SousSet device 1.0");

	if(IsFTCflagSet()){
		print("SmartConfig Set");
	} else {
		print("SmartConfig not Set");
	}


	//checkWiFiConnected();
	__delay_cycles(6000000);
	if(IsFTCflagSet()){
		//smartStartup();

	} else {
		startSmartConfig(0);
		print("Saving to flash");
		SetFTCflag();
		print("Saved..");
	}
	if(IsFTCflagSet()){
			print("SmartConfig Set");
		} else {
			print("SmartConfig not Set");
		}
	__delay_cycles(6000000);

	if(checkDHCP()){
		print("CC3000 DHCP complete");
		char ipStr[15];
		sprintf( (char*)ipStr,"%d.%d.%d.%d", pucCC3000_Rx_Buffer[3],pucCC3000_Rx_Buffer[2], pucCC3000_Rx_Buffer[1], pucCC3000_Rx_Buffer[0] );
		print(ipStr);
	}
	print("Setup Done...");
}

/**
 * loop() - this routine runs over and over
 *
 */
void loop(){
	P1OUT ^= (LED_GREEN_B+LED_RED_B);// board blinks per loop
	__delay_cycles(6000000);


	//print(GetTempHex());

}

/**
 * main - sample program main loop
 *
 */

void main(void)
{
    setup();

    while( 1 ) {
        loop();
    }
}
